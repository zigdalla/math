# extract.py
# find the <math tags
import sys
from bs4 import BeautifulSoup
f = sys.argv[1]
import glob

for f in (glob.glob(f)):
    soup = BeautifulSoup(open(f), features="html5lib")
    for link in soup.find_all('math'):
        print(link)
