grep -c -i "doctype html" * | grep -v :0 | cut -d: -f1 > math.txt

input="math.txt"

while IFS= read -r page
do
    echo $page;

    if [ ! -f "${page}.org"  ]; then
	pandoc -f html-native_divs "$page" -o "$page.org"

    fi

done < "$input"

