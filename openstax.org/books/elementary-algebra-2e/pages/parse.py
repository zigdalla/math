# extract.py
import sys
from bs4 import BeautifulSoup
import pprint
import glob

class Name:
    def __init__(self, n, depth):
        self.n = n
        self.depth= depth
        
    def count(self, c):
        c = c['name']
        if self.n not in c:
            c[self.n] =1
        else:
            c[self.n] = c[self.n] + 1

class Branch:
    def __init__(self, n, depth, width):
        self.n = n
        self.depth= depth
        self.width = width
        
    def count(self, c):

        d = c['depth']
        
        c = c['brnch']
        dn = str(self.depth) + ":" + str(self.width)
        
        if dn not in d:
            d[dn] =1
        else:
            d[dn] = d[dn] + 1

        # now peg the field
        n = self.n
        if self.n not in c:
            c[n] =1
        else:
            c[n] = c[n] + 1

class Leaf:
    def __init__(self, n , v, depth):
        self.n = n
        self.v = v
        self.depth= depth
        
    def count(self, c):
        
        c2 = c['node']

        if self.n not in c2:
            c2[self.n] =1
        else:
            c2[self.n] = c2[self.n] + 1

        # count the values
        v = c['value']
        v2 = self.v
        if v2 not in v:
            v[v2] =1
        else:
            v[v2] = v[v2] + 1

def recmath(x, i=0):
    ind = " " * i
    
    for y in x:
        
        #if y.name is None:
        #    if str(y).strip() == "": # ws
        #        pass
        #    else:
        #        #print(ind + "NONE?" +"|"+ y + "|") # the variable name
        #        pass
        if y.name is None :
            pass
        elif y.name in('annotation-xml'):
            pass
        elif y.name in( "mi", "mo", "mstyle", "mspace", "mtext"):
            #print(ind + y.name +"|"+ y.get_text()) # the variable name
            yield Leaf(y.name, y.get_text(), i)
        
        elif y.name in( "mn"):
            for z in y:
                yield Name(z, i)

        elif y.name in( "mrow", "mtable", "mtr", "mtd","mfrac","menclose", "msqrt", "munder", "msup","msub", "mover", "mfenced", "mroot"):
            #print(ind + y.name)
            yield Branch(y.name, i, len(y))
            yield from recmath(y,i+1)
        else:
            #print ("TODO" + y.name)
            yield from recmath(y,i+1)

f = sys.argv[1]
fi = open(f)

cnt = {
    'name'  :  {},
    'brnch' : {},
    'node'  :  {},
    'value' : {},
    'depth' : {},
}

for f in fi:
    f = f.strip()
    print(f)
    soup = BeautifulSoup(open(f), features="html5lib")
    count = 1
    for link in soup.find_all('math'):
        #print (f + " " + str(count))
        count = count + 1
        for x in link.semantics:
            for x in recmath(x):
                x.count(cnt)
pprint.pprint(cnt)
