[[#main-content][Skip to Content]]

[[/][[[/rex/releases/v3/7dbf61c/static/media/logo.b5e6d030.svg]]]]

Elementary Algebra 2e

* Key Concepts
  :PROPERTIES:
  :CUSTOM_ID: key-concepts
  :CLASS: BookBanner__BookChapter-sc-1avy0c0-4 hQlFIi
  :END:

Elementary Algebra 2eKey Concepts

Table of contents

My highlights

Print

Table of contents

1.  [[file:preface][Preface]]
2.  1 Foundations

    1.  [[file:1-introduction][Introduction]]
    2.  [[file:1-1-introduction-to-whole-numbers][1.1 Introduction to
        Whole Numbers]]
    3.  [[file:1-2-use-the-language-of-algebra][1.2 Use the Language of
        Algebra]]
    4.  [[file:1-3-add-and-subtract-integers][1.3 Add and Subtract
        Integers]]
    5.  [[file:1-4-multiply-and-divide-integers][1.4 Multiply and Divide
        Integers]]
    6.  [[file:1-5-visualize-fractions][1.5 Visualize Fractions]]
    7.  [[file:1-6-add-and-subtract-fractions][1.6 Add and Subtract
        Fractions]]
    8.  [[file:1-7-decimals][1.7 Decimals]]
    9.  [[file:1-8-the-real-numbers][1.8 The Real Numbers]]
    10. [[file:1-9-properties-of-real-numbers][1.9 Properties of Real
        Numbers]]
    11. [[file:1-10-systems-of-measurement][1.10 Systems of
        Measurement]]
    12. [[file:1-key-terms][Key Terms]]
    13. [[file:1-key-concepts][Key Concepts]]
    14. Exercises

        1. [[file:1-review-exercises][Review Exercises]]
        2. [[file:1-practice-test][Practice Test]]

3.  2 Solving Linear Equations and Inequalities

    1.  [[file:2-introduction][Introduction]]
    2.  [[file:2-1-solve-equations-using-the-subtraction-and-addition-properties-of-equality][2.1
        Solve Equations Using the Subtraction and Addition Properties of
        Equality]]
    3.  [[file:2-2-solve-equations-using-the-division-and-multiplication-properties-of-equality][2.2
        Solve Equations using the Division and Multiplication Properties
        of Equality]]
    4.  [[file:2-3-solve-equations-with-variables-and-constants-on-both-sides][2.3
        Solve Equations with Variables and Constants on Both Sides]]
    5.  [[file:2-4-use-a-general-strategy-to-solve-linear-equations][2.4
        Use a General Strategy to Solve Linear Equations]]
    6.  [[file:2-5-solve-equations-with-fractions-or-decimals][2.5 Solve
        Equations with Fractions or Decimals]]
    7.  [[file:2-6-solve-a-formula-for-a-specific-variable][2.6 Solve a
        Formula for a Specific Variable]]
    8.  [[file:2-7-solve-linear-inequalities][2.7 Solve Linear
        Inequalities]]
    9.  [[file:2-key-terms][Key Terms]]
    10. [[file:2-key-concepts][Key Concepts]]
    11. Exercises

        1. [[file:2-review-exercises][Review Exercises]]
        2. [[file:2-practice-test][Practice Test]]

4.  3 Math Models

    1.  [[file:3-introduction][Introduction]]
    2.  [[file:3-1-use-a-problem-solving-strategy][3.1 Use a
        Problem-Solving Strategy]]
    3.  [[file:3-2-solve-percent-applications][3.2 Solve Percent
        Applications]]
    4.  [[file:3-3-solve-mixture-applications][3.3 Solve Mixture
        Applications]]
    5.  [[file:3-4-solve-geometry-applications-triangles-rectangles-and-the-pythagorean-theorem][3.4
        Solve Geometry Applications: Triangles, Rectangles, and the
        Pythagorean Theorem]]
    6.  [[file:3-5-solve-uniform-motion-applications][3.5 Solve Uniform
        Motion Applications]]
    7.  [[file:3-6-solve-applications-with-linear-inequalities][3.6
        Solve Applications with Linear Inequalities]]
    8.  [[file:3-key-terms][Key Terms]]
    9.  [[file:3-key-concepts][Key Concepts]]
    10. Exercises

        1. [[file:3-review-exercises][Review Exercises]]
        2. [[file:3-practice-test][Practice Test]]

5.  4 Graphs

    1.  [[file:4-introduction][Introduction]]
    2.  [[file:4-1-use-the-rectangular-coordinate-system][4.1 Use the
        Rectangular Coordinate System]]
    3.  [[file:4-2-graph-linear-equations-in-two-variables][4.2 Graph
        Linear Equations in Two Variables]]
    4.  [[file:4-3-graph-with-intercepts][4.3 Graph with Intercepts]]
    5.  [[file:4-4-understand-slope-of-a-line][4.4 Understand Slope of a
        Line]]
    6.  [[file:4-5-use-the-slope-intercept-form-of-an-equation-of-a-line][4.5
        Use the Slope-Intercept Form of an Equation of a Line]]
    7.  [[file:4-6-find-the-equation-of-a-line][4.6 Find the Equation of
        a Line]]
    8.  [[file:4-7-graphs-of-linear-inequalities][4.7 Graphs of Linear
        Inequalities]]
    9.  [[file:4-key-terms][Key Terms]]
    10. [[file:4-key-concepts][Key Concepts]]
    11. Exercises

        1. [[file:4-review-exercises][Review Exercises]]
        2. [[file:4-practice-test][Practice Test]]

6.  5 Systems of Linear Equations

    1.  [[file:5-introduction][Introduction]]
    2.  [[file:5-1-solve-systems-of-equations-by-graphing][5.1 Solve
        Systems of Equations by Graphing]]
    3.  [[file:5-2-solving-systems-of-equations-by-substitution][5.2
        Solving Systems of Equations by Substitution]]
    4.  [[file:5-3-solve-systems-of-equations-by-elimination][5.3 Solve
        Systems of Equations by Elimination]]
    5.  [[file:5-4-solve-applications-with-systems-of-equations][5.4
        Solve Applications with Systems of Equations]]
    6.  [[file:5-5-solve-mixture-applications-with-systems-of-equations][5.5
        Solve Mixture Applications with Systems of Equations]]
    7.  [[file:5-6-graphing-systems-of-linear-inequalities][5.6 Graphing
        Systems of Linear Inequalities]]
    8.  [[file:5-key-terms][Key Terms]]
    9.  [[file:5-key-concepts][Key Concepts]]
    10. Exercises

        1. [[file:5-review-exercises][Review Exercises]]
        2. [[file:5-practice-test][Practice Test]]

7.  6 Polynomials

    1.  [[file:6-introduction][Introduction]]
    2.  [[file:6-1-add-and-subtract-polynomials][6.1 Add and Subtract
        Polynomials]]
    3.  [[file:6-2-use-multiplication-properties-of-exponents][6.2 Use
        Multiplication Properties of Exponents]]
    4.  [[file:6-3-multiply-polynomials][6.3 Multiply Polynomials]]
    5.  [[file:6-4-special-products][6.4 Special Products]]
    6.  [[file:6-5-divide-monomials][6.5 Divide Monomials]]
    7.  [[file:6-6-divide-polynomials][6.6 Divide Polynomials]]
    8.  [[file:6-7-integer-exponents-and-scientific-notation][6.7
        Integer Exponents and Scientific Notation]]
    9.  [[file:6-key-terms][Key Terms]]
    10. [[file:6-key-concepts][Key Concepts]]
    11. Exercises

        1. [[file:6-review-exercises][Review Exercises]]
        2. [[file:6-practice-test][Practice Test]]

8.  7 Factoring

    1.  [[file:7-introduction][Introduction]]
    2.  [[file:7-1-greatest-common-factor-and-factor-by-grouping][7.1
        Greatest Common Factor and Factor by Grouping]]
    3.  [[file:7-2-factor-trinomials-of-the-form-x2-bx-c][7.2 Factor
        Trinomials of the Form x2+bx+c]]
    4.  [[file:7-3-factor-trinomials-of-the-form-ax2-bx-c][7.3 Factor
        Trinomials of the Form ax2+bx+c]]
    5.  [[file:7-4-factor-special-products][7.4 Factor Special
        Products]]
    6.  [[file:7-5-general-strategy-for-factoring-polynomials][7.5
        General Strategy for Factoring Polynomials]]
    7.  [[file:7-6-quadratic-equations][7.6 Quadratic Equations]]
    8.  [[file:7-key-terms][Key Terms]]
    9.  [[file:7-key-concepts][Key Concepts]]
    10. Exercises

        1. [[file:7-review-exercises][Review Exercises]]
        2. [[file:7-practice-test][Practice Test]]

9.  8 Rational Expressions and Equations

    1.  [[file:8-introduction][Introduction]]
    2.  [[file:8-1-simplify-rational-expressions][8.1 Simplify Rational
        Expressions]]
    3.  [[file:8-2-multiply-and-divide-rational-expressions][8.2
        Multiply and Divide Rational Expressions]]
    4.  [[file:8-3-add-and-subtract-rational-expressions-with-a-common-denominator][8.3
        Add and Subtract Rational Expressions with a Common
        Denominator]]
    5.  [[file:8-4-add-and-subtract-rational-expressions-with-unlike-denominators][8.4
        Add and Subtract Rational Expressions with Unlike Denominators]]
    6.  [[file:8-5-simplify-complex-rational-expressions][8.5 Simplify
        Complex Rational Expressions]]
    7.  [[file:8-6-solve-rational-equations][8.6 Solve Rational
        Equations]]
    8.  [[file:8-7-solve-proportion-and-similar-figure-applications][8.7
        Solve Proportion and Similar Figure Applications]]
    9.  [[file:8-8-solve-uniform-motion-and-work-applications][8.8 Solve
        Uniform Motion and Work Applications]]
    10. [[file:8-9-use-direct-and-inverse-variation][8.9 Use Direct and
        Inverse Variation]]
    11. [[file:8-key-terms][Key Terms]]
    12. [[file:8-key-concepts][Key Concepts]]
    13. Exercises

        1. [[file:8-review-exercises][Review Exercises]]
        2. [[file:8-practice-test][Practice Test]]

10. 9 Roots and Radicals

    1.  [[file:9-introduction][Introduction]]
    2.  [[file:9-1-simplify-and-use-square-roots][9.1 Simplify and Use
        Square Roots]]
    3.  [[file:9-2-simplify-square-roots][9.2 Simplify Square Roots]]
    4.  [[file:9-3-add-and-subtract-square-roots][9.3 Add and Subtract
        Square Roots]]
    5.  [[file:9-4-multiply-square-roots][9.4 Multiply Square Roots]]
    6.  [[file:9-5-divide-square-roots][9.5 Divide Square Roots]]
    7.  [[file:9-6-solve-equations-with-square-roots][9.6 Solve
        Equations with Square Roots]]
    8.  [[file:9-7-higher-roots][9.7 Higher Roots]]
    9.  [[file:9-8-rational-exponents][9.8 Rational Exponents]]
    10. [[file:9-key-terms][Key Terms]]
    11. [[file:9-key-concepts][Key Concepts]]
    12. Exercises

        1. [[file:9-review-exercises][Review Exercises]]
        2. [[file:9-practice-test][Practice Test]]

11. 10 Quadratic Equations

    1. [[file:10-introduction][Introduction]]
    2. [[file:10-1-solve-quadratic-equations-using-the-square-root-property][10.1
       Solve Quadratic Equations Using the Square Root Property]]
    3. [[file:10-2-solve-quadratic-equations-by-completing-the-square][10.2
       Solve Quadratic Equations by Completing the Square]]
    4. [[file:10-3-solve-quadratic-equations-using-the-quadratic-formula][10.3
       Solve Quadratic Equations Using the Quadratic Formula]]
    5. [[file:10-4-solve-applications-modeled-by-quadratic-equations][10.4
       Solve Applications Modeled by Quadratic Equations]]
    6. [[file:10-5-graphing-quadratic-equations-in-two-variables][10.5
       Graphing Quadratic Equations in Two Variables]]
    7. [[file:10-key-terms][Key Terms]]
    8. [[file:10-key-concepts][Key Concepts]]
    9. Exercises

       1. [[file:10-review-exercises][Review Exercises]]
       2. [[file:10-practice-test][Practice Test]]

12. Answer Key

    1.  [[file:chapter-1][Chapter 1]]
    2.  [[file:chapter-2][Chapter 2]]
    3.  [[file:chapter-3][Chapter 3]]
    4.  [[file:chapter-4][Chapter 4]]
    5.  [[file:chapter-5][Chapter 5]]
    6.  [[file:chapter-6][Chapter 6]]
    7.  [[file:chapter-7][Chapter 7]]
    8.  [[file:chapter-8][Chapter 8]]
    9.  [[file:chapter-9][Chapter 9]]
    10. [[file:chapter-10][Chapter 10]]

13. [[file:index][Index]]

[[file:4-1-use-the-rectangular-coordinate-system#0][]]

*** 4.1 Use the Rectangular Coordinate System
    :PROPERTIES:
    :CUSTOM_ID: 0_copy_1
    :data-type: document-title
    :END:

- *Sign Patterns of the Quadrants*\\
   $\begin{array}{lcclcclccl}
  \text{Quadrant\ I} & & & \text{Quadrant\ II} & & & \text{Quadrant\ III} & & & \text{Quadrant\ IV} \\
  {(x,y)} & & & {(x,y)} & & & {(x,y)} & & & {(x,y)} \\
  {( + , + )} & & & {(\text{−}, + )} & & & {(\text{−},\text{−})} & & & {( + ,\text{−})} \\
  \end{array}$
- *Points on the Axes*\\

  - On the /x/-axis, $y = 0$. Points with a /y/-coordinate equal to 0
    are on the /x/-axis, and have coordinates $(a,0)$.
  - On the /y/-axis, $x = 0$. Points with an /x/-coordinate equal to 0
    are on the /y/-axis, and have coordinates ${(0,b)}.$

- *Solution of a Linear Equation*\\

  - An ordered pair $\left( {x,y} \right)$ is a solution of the linear
    equation $Ax + By = C$, if the equation is a true statement when the
    /x/- and /y/- values of the ordered pair are substituted into the
    equation.

[[file:4-2-graph-linear-equations-in-two-variables#0][]]

*** 4.2 Graph Linear Equations in Two Variables
    :PROPERTIES:
    :CUSTOM_ID: 0_copy_10
    :data-type: document-title
    :END:

- *Graph a Linear Equation by Plotting Points*

  1. Step 1. Find three points whose coordinates are solutions to the
     equation. Organize them in a table.
  2. Step 2. Plot the points in a rectangular coordinate system. Check
     that the points line up. If they do not, carefully check your work!
  3. Step 3. Draw the line through the three points. Extend the line to
     fill the grid and put arrows on both ends of the line.

[[file:4-3-graph-with-intercepts#0][]]

*** 4.3 Graph with Intercepts
    :PROPERTIES:
    :CUSTOM_ID: 0_copy_11
    :data-type: document-title
    :END:

- *Find the /x/- and /y/- Intercepts from the Equation of a Line*

  - Use the equation of the line to find the /x/- intercept of the line,
    let $y = 0$ and solve for /x/.
  - Use the equation of the line to find the /y/- intercept of the line,
    let $x = 0$ and solve for /y/.

- *Graph a Linear Equation using the Intercepts*

  1. Step 1. Find the /x/- and /y/- intercepts of the line.\\
      Let $y = 0$ and solve for /x/.\\
      Let $x = 0$ and solve for /y/.
  2. Step 2. Find a third solution to the equation.
  3. Step 3. Plot the three points and then check that they line up.
  4. Step 4. Draw the line.

  \\
  \\

- *Strategy for Choosing the Most Convenient Method to Graph a Line:*

  - Consider the form of the equation.
  - If it only has one variable, it is a vertical or horizontal line.\\
     $x = a$ is a vertical line passing through the /x/- axis at $a$\\
     $y = b$ is a horizontal line passing through the /y/- axis at $b$.
  - If /y/ is isolated on one side of the equation, graph by plotting
    points.
  - Choose any three values for /x/ and then solve for the corresponding
    /y/- values.
  - If the equation is of the form $ax + by = c$, find the intercepts.
    Find the /x/- and /y/- intercepts and then a third point.

[[file:4-4-understand-slope-of-a-line#0][]]

*** 4.4 Understand Slope of a Line
    :PROPERTIES:
    :CUSTOM_ID: 0_copy_12
    :data-type: document-title
    :END:

- *Find the Slope of a Line from its Graph using*
  $m = \frac{\text{rise}}{\text{run}}$

  1. Step 1. Locate two points on the line whose coordinates are
     integers.
  2. Step 2. Starting with the point on the left, sketch a right
     triangle, going from the first point to the second point.
  3. Step 3. Count the rise and the run on the legs of the triangle.
  4. Step 4. Take the ratio of rise to run to find the slope.

  \\
  \\

- *Graph a Line Given a Point and the Slope*

  1. Step 1. Plot the given point.
  2. Step 2. Use the slope formula $m = \frac{\text{rise}}{\text{run}}$
     to identify the rise and the run.
  3. Step 3. Starting at the given point, count out the rise and run to
     mark the second point.
  4. Step 4. Connect the points with a line.\\

  \\
  \\

- *Slope of a Horizontal Line*

  - The slope of a horizontal line, $y = b$, is 0.

- *Slope of a vertical line*

  - The slope of a vertical line, $x = a$, is undefined

[[file:4-5-use-the-slope-intercept-form-of-an-equation-of-a-line#0][]]

*** 4.5 Use the Slope-Intercept Form of an Equation of a Line
    :PROPERTIES:
    :CUSTOM_ID: 0_copy_13
    :data-type: document-title
    :END:

- The slope--intercept form of an equation of a line with slope $m$ and
  /y/-intercept, $\left( {0,b} \right)$ is, $y = mx + b$.
- *Graph a Line Using its Slope and /y/-Intercept*

  1. Step 1. Find the slope-intercept form of the equation of the line.
  2. Step 2. Identify the slope and /y/-intercept.
  3. Step 3. Plot the /y/-intercept.
  4. Step 4. Use the slope formula $m = \frac{\text{rise}}{\text{run}}$
     to identify the rise and the run.
  5. Step 5. Starting at the /y/-intercept, count out the rise and run
     to mark the second point.
  6. Step 6. Connect the points with a line.

  \\
  \\

- *Strategy for Choosing the Most Convenient Method to Graph a Line:*
  Consider the form of the equation.

  - If it only has one variable, it is a vertical or horizontal line.\\
    $x = a$ is a vertical line passing through the /x/-axis at $a$.\\
    $y = b$ is a horizontal line passing through the /y/-axis at $b$.
  - If $y$ is isolated on one side of the equation, in the form
    $y = mx + b$, graph by using the slope and /y/-intercept.\\
     Identify the slope and /y/-intercept and then graph.
  - If the equation is of the form $Ax + By = C$, find the intercepts.\\
     Find the /x/- and /y/-intercepts, a third point, and then graph.

- Parallel lines are lines in the same plane that do not intersect.

  - Parallel lines have the same slope and different /y/-intercepts.
  - If /m/_{1} and /m/_{2} are the slopes of two parallel lines then
    $m_{1} = m_{2}.$
  - Parallel vertical lines have different /x/-intercepts.

- Perpendicular lines are lines in the same plane that form a right
  angle.

  - If $m_{1}\ \text{and}\ m_{2}$ are the slopes of two perpendicular
    lines, then $m_{1} \cdot m_{2} = -1$ and $m_{1} = \frac{-1}{m_{2}}$.
  - Vertical lines and horizontal lines are always perpendicular to each
    other.

[[file:4-6-find-the-equation-of-a-line#0][]]

*** 4.6 Find the Equation of a Line
    :PROPERTIES:
    :CUSTOM_ID: 0_copy_14
    :data-type: document-title
    :END:

- *To Find an Equation of a Line Given the Slope and a Point*

  1. Step 1. Identify the slope.
  2. Step 2. Identify the point.
  3. Step 3. Substitute the values into the point-slope form,
     $y - y_{1} = m\left( {x - x_{1}} \right)$.
  4. Step 4. Write the equation in slope-intercept form.

  \\
  \\

- *To Find an Equation of a Line Given Two Points*

  1. Step 1. Find the slope using the given points.
  2. Step 2. Choose one point.
  3. Step 3. Substitute the values into the point-slope form,
     $y - y_{1} = m\left( {x - x_{1}} \right)$.
  4. Step 4. Write the equation in slope-intercept form.

  \\
  \\

- *To Write and Equation of a Line*

  - If given slope and /y/-intercept, use slope--intercept form
    $y = mx + b$.
  - If given slope and a point, use point--slope form
    $y - y_{1} = m\left( {x - x_{1}} \right)$.
  - If given two points, use point--slope form
    $y - y_{1} = m\left( {x - x_{1}} \right)$.

  \\
  \\

- *To Find an Equation of a Line Parallel to a Given Line*

  1. Step 1. Find the slope of the given line.
  2. Step 2. Find the slope of the parallel line.
  3. Step 3. Identify the point.
  4. Step 4. Substitute the values into the point-slope form,
     $y - y_{1} = m\left( {x - x_{1}} \right)$.
  5. Step 5. Write the equation in slope-intercept form.

  \\
  \\

- *To Find an Equation of a Line Perpendicular to a Given Line*

  1. Step 1. Find the slope of the given line.
  2. Step 2. Find the slope of the perpendicular line.
  3. Step 3. Identify the point.
  4. Step 4. Substitute the values into the point-slope form,
     $y - y_{1} = m\left( {x - x_{1}} \right)$.
  5. Step 5. Write the equation in slope-intercept form.

[[file:4-7-graphs-of-linear-inequalities#0][]]

*** 4.7 Graphs of Linear Inequalities
    :PROPERTIES:
    :CUSTOM_ID: 0_copy_15
    :data-type: document-title
    :END:

- *To Graph a Linear Inequality*

  1. Step 1. Identify and graph the boundary line.\\
      If the inequality is $\leq \text{or} \geq$, the boundary line is
     solid.\\
      If the inequality is < or >, the boundary line is dashed.
  2. Step 2. Test a point that is not on the boundary line. Is it a
     solution of the inequality?
  3. Step 3. Shade in one side of the boundary line.\\
      If the test point is a solution, shade in the side that includes
     the point.\\
      If the test point is not a solution, shade in the opposite side.

PreviousNext

[[https://amzn.to/37wiIrQ][Order a print copy]]

As an Amazon Associate we earn from qualifying purchases.

Citation/Attribution

Want to cite, share, or modify this book? This book is Creative Commons
Attribution License 4.0 and you must attribute OpenStax.

*Attribution information*

- If you are redistributing all or part of this book in a print format,
  then you must include on every physical page the following
  attribution:

  #+BEGIN_QUOTE
    Access for free at
    https://openstax.org/books/elementary-algebra-2e/pages/1-introduction
  #+END_QUOTE

- If you are redistributing all or part of this book in a digital
  format, then you must include on every digital page view the following
  attribution:

  #+BEGIN_QUOTE
    Access for free at
    [[https://openstax.org/books/elementary-algebra-2e/pages/1-introduction]]
  #+END_QUOTE

*Citation information*

- Use the information below to generate a citation. We recommend using a
  citation tool such as
  [[https://www.lib.ncsu.edu/citationbuilder/#/default/default][this
  one]].

  - Authors: Lynn Marecek, MaryAnne Anthony-Smith, Andrea Honeycutt
    Mathis
  - Publisher/website: OpenStax
  - Book title: Elementary Algebra 2e
  - Publication date: Apr 22, 2020
  - Location: Houston, Texas
  - Book URL:
    [[https://openstax.org/books/elementary-algebra-2e/pages/1-introduction]]
  - Section URL:
    [[https://openstax.org/books/elementary-algebra-2e/pages/4-key-concepts]]

© Dec 15, 2020 OpenStax. Textbook content produced by OpenStax is
licensed under a Creative Commons Attribution License 4.0 license. *The
OpenStax name, OpenStax logo, OpenStax book covers, OpenStax CNX name,
and OpenStax CNX logo are not subject to the Creative Commons license
and may not be reproduced without the prior and express written consent
of Rice University.*

Our mission is to improve educational access and learning for everyone.

OpenStax is part of Rice University, which is a 501(c)(3) nonprofit.
[[/give][Give today]] and help us reach more students.

Help

[[/contact][Contact
Us]][[https://openstax.secure.force.com/help][Support
Center]][[/faq][FAQ]]

OpenStax

[[/press][Press]][[http://www2.openstax.org/l/218812/2016-10-04/lvk][Newsletter]][[/careers][Careers]]

Policies

[[/accessibility-statement][Accessibility Statement]][[/tos][Terms of
Use]][[/license][Licensing]][[/privacy-policy][Privacy Policy]]

© 1999-2021, Rice University. Except where otherwise noted, textbooks on
this site are licensed under a
[[https://creativecommons.org/licenses/by/4.0/][Creative Commons
Attribution 4.0 International License]].

Advanced Placement^{®} and AP^{®} are trademarks registered and/or owned
by the College Board, which is not affiliated with, and does not
endorse, this site.

[[http://www.rice.edu][[[data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASwAAAB4CAMAAAB/yz8SAAAC91BMVEUAAAD///8DAwP///////////////////////80Okn///////////////////////////////////////////////////8MCAz///8IBwj///////////////////8DAQIKCQsZFh3///8aGB0NDA5SVmf///////////////////////////9OUF3///8JBQdHU2n///////////8WFh1GRk+enq4vLzsgITApMD1ranwxM0EqJjANDRV6e5RLV3ZHSV5vcIxCSV8kKTNZVmpMSWI7QFKWmJ9FRFb////y8u/29/Lx8e309O/p6ef///vs7On19fHv7+z29/RukbAnNkrm5uEoNEQxTG8nP1cmPFInMD7j4+A2UHEmOU4wTXMvS2ssSGr+/fjm5uWjt8I2VHwAAAFWaJEzUHfa2dcnQlwoRGGysrw6V4ApR2bz8uzu7une3dpYcZ01TWzCwsVeg6nU1dZFWHjHyMVncYy4uLZljK0BAQxnk7IIFypOWnnf395slrNkj7C/v7xdd6FUbJkaHyZbfKUuSGQFDx/7+/XOzsxgiKxoeJk+XIY+T2nJyM1rhqeenpxFYYz4+PS5ucOErL5xl7Rti61XdqFVYoM1RVpvnbeYl5YsSG/Pz9ELIDqIipA7S2I7RFNzn7isrKhOapZlc5RbaIeEg4YDCBVigKRbb5ZTYYvV1c18qL13pLtRYH5NVnNfYGAbK0ApKi2rqrmRkZQ/VHGhoqNqdpJ3eHhIUFwbMVSoucKNrr4MJklpmbVfbpB9foJHXX8BFzgSFRqwsLJpfZ5fa4xOZYgjOVySsb9mga+npqNnZ2c2PFowPVYjM0eOjYlXVlZOTU7t7OSYsb61tK9edppLZpI5UnVCQkQ3Nja5v8qrvMh5hKiVlqFTW4AYJDdtb3lwb21zfaFyeINhZnBee6pTWmXNzMWDjKsYK0ekpLZ5mrShsLmGjJ9RbYtRZHN2g5OXtsZyjLeRpbCjpqwjPWd8mL2Bo7OMl6/YTXWhAAAASXRSTlMA3Qj4FiZ9Yln9bS7t5HXN2AxOx4RDHoMGFDXTpWc8XR9Qiz4tlvO9Ep9Jtap/kHH+r5qVrF3+h+ndw65jvf3iuubRxq3p69iVcwgPfQAAHXNJREFUeNrs1LFqwmAUhuHg1kvIpvxxFFysEIMa0eIFuPUC3AURkTRElC4GcdLaDhKEBtSh0iGEdikOtWO3bqIQOgRKHBykQ89/tGnr1DnmCckFvJx8jM/n8/l8vn8LsEGOkJCLEC7IsgHG52I5IsTz6XQmlerr81vVAos9yzJFkeez+YhAgkddLcCFhHg6k3vXdX0+L47HpVKjcVkut2p1SZIU0O12P9B2a9uWyGfD0Iw5NgEOMuWmSyCDZX8yKRaLTayFuWr1GrxSoVBQugi7bTa2nRSzEcIyx4LEE7moLFfAlHJr7U5rhLEQ1nL18NPrLOxFkg/HvB8sKCRy0MeooCmCVhDrsFW9Tt8/sagOglWzHT7CMd7FComHaNSgpYyDWHBYTfgL6U+Io4UkUFDgOaj1TKmm5TgiH/Po7MfPom2tbQBMJf/EoocFsdzDgk5I2aHDdQOjtS9Ga6mqalJJ59STi09ONK0NXi8G94PHijYYnK9W1zLAw4J5v9qtewtbKb8Mh2+z9dO6vB3N7vTZy6jzHatqVj9jjAd9cVu3oUnEcQDHtcymTjebxpi1oBfr+Rl62fvfVp49IFQWZGEIPWzM1nU9dxQsX1RW61HGrVndIjcws8NFlCOLRIvNtFiRpbiyiGqDQW/633lzc3n1phfhF/Zm7L/tPvf7nTdDdgppvTgko3wU86njQzrIEJTszgjW8RwWeq6zWDmuC+3rfqSHncyOwcYfDEnYlt/czmqxVvv2FS1WW1vblY7mYQAXQN8nMNsB3P1f3qMKDNbDh+il4Spb1zrZxvNgDENdlAKjGyCDtLjR2oe4Phct1pWO6OlAwJ5M27G4g6Fxcve57he5LbyUG6yHbJxUe3vXV1mKTCZsAwnrbobwUdaM+eXNV+xoIarixTrVdufADqijk0HngB0P4MEkCdB65zDCQlYs1p4sFk/FWl1t7/zhbQVTkgrSAVOcanXRVoDzPdmnVrFiTWCxDj1O0wx1HsAVcDYBEL0Ebu1uvJ3FyhusLBWqq1PmXUExPvTjPiqsR0cZvMF+s7ixuMlq7qDjdn8CIOjsHQaIE4YGhJXbwj0jWLxUF2v1c+OJBGFLnoe1aHGRcxDXmzI3byGtG8WNdchyFxqc6YTDZ7UGEmTYuRIyG3v4z0L+8Y6s+JniammRNTeBm0oPOxgTTgczYRcAufcMi3WjWLG4Ndxs8SZ8RrcvTgBkyPC5Jpf1TfP226/zthDtIAfVydXSkrbIlifqSEfcBqsyBInhNve1sxefPClyrFNtHyyW5WAngdhqdwOhx5wex8D97duvs1vIvzdkrTgnvm5Lx1uw2bHgWls9EGCmI+HB62f+Dyy1ZFKVFlWtmCRR/0usTY2s1okkRbs8TrrPGfBHUgRgn54dHzdYyGqU6kHoZ7dlI03TYQ+Tijj9/ogfh5dPr7NYNxDWAtHYJLNnqysrRaNVqtWz0fdEf6tSwh7MP/bHA5K5qqWzxKWQa2KpuGz+TLlWMp4T/WZ1pUiw2QqNphBW4yak5Y329fX3Rb2RAO2ymk/Cye5dyCr3QooGi7d6kC00NNR9oL/P4+n39HtTATpoxE6C8fLxJwWwpgGbUikuL6uo0JWLlcrS0hpUqbK8YpqmZKqwlTh7sBwdLEPnOIQpIqG0M8smglDieZrp2pE/NRXYarL/El8Znw5VrqwB0BXGuvfiQIRxML6U34PX1tatJt0YGF/sZF9IeSs0WJ1dnFUo9CCU1Up7na5hF+WPRBr0K1fpSZMZWntecXv4Lg+rumSydBYIVyGdJCrc3MmqeXlXvEwuZDtVqoO/p9TNYcdJPaVEvqwc/pxqPNai5yzWpsYD0VYbTtqIaBxq61fjvQ0Y4OuPc4N1DGGxVvu//kRaLFO2oW8naJPdjreGo/ZzBv3uuLMeg2BPASwuybwxu6HUoVsohtGmKYQ3a/SipCKhFHNyHLOWqkrmVlcpFIoqbYlqTkUp5KcevRUT4U9phbCaH+OmhiNba1MpbLdBj2V6TQDUlo/cYB08ylp9H1zTtK4lxGHFYrFQbCjmja4xmrbWGqK9mN6oN8cdqwAGzxbGQikhm6pKwg+DVjM6DTOFIXITKOi5lL8J8+RVvz/tq6UVkCtvuUqAP6eaPF0uV0mlmpnL5iwZ4Z0kgLVpgyVVZzQaz+H9BNQb9ID3GqF+8XV2sA6uu9De2fI9vRKg6euDoRAqtm1bDH0NdATNBsMaCL9xs1qYy1EHTT2vbjwqjLWQXyXR2KbkLqRM8NGty+1F4abXZBnkEkFvjRL4lojGxLOIxz0qqzVo5mrUhbC40frFq50/xRjHcQDPoELuc4gfzLiPcfwVn2d3e9pKGqQ89JQhm1zZ3HtgEW1LG5N7I5EjK1eYsM6tMbspaRnjDJEz1w8+3+d5lsU+PE+p95h+WLPT02ven89+d/c5wKappuAf7PIUghq1dK4IKI4jxYpayVlZINICK76e/1BJMoOkweFRU2Fharq8BmmxW2VmrNZ1Mawg4NLlt4cDfwyQ2KtjKGDEV/tIIAkJ+vvraidh5ob5PtrdLxYpfQg+KNKsu7G3WI+Sio7WKM54bGoAFaNzRc7duBo/eY/KPrzl6EUTREbyWpxT5Yxz+PMRexGU4eFMRFFNlZqmKYutDOYv2ld6zS/WcJErC/7XmI0DPgP8Lna+d6FCL8XTi1+a430fGyuGhRfVTwwLtY6zRWAJi1YwZne5qyyMZtKqNE+r16PVkcPv8mlQRHJa0xtuIpOQ+hr2E0SmpNAmV0W5y0aZFNq0KddKS0v9Yg0Qu7Lgf+ytgcCng78B4yepf4CEjP/jdwwTxeoj1ixSrbuxRlxbtCrCAilac3lFGJiio3clI9beI+92AVrxgbCGyvuE6xz50WBnTwFY1DSEXTG7ixhaMWXu+0MiWH1Fr6wHCBHWs8igtvVj1VoYUEkh3Qr81xoVMvgvWAl35z1kPfm6FdEpJkZts3vmggJO3V21NPv1IrRSIxYfCK+v99HysBW5O6aHRyJnscNN4ZPeXJKJhWnlXfIB/tIJ+PT6c7QGCy/yEhPy26vEQPFLEt9ZP7Tc7iJMxZnCfI8OQHdyVXb2yV1AqxWKn1oRqIVS+I9oVRyowOeUlxe5c28/RmL4tEhkZ3UQv7LhIDppmPZiWH06AkmwVCvC3l4i1oiBos1CrOTYh/bCQpstN7fKXGHWlRvLIO3kypXESoVWf2qRHX+/8mwRU6jTmsvSzOWFhW57mhQs8WqNloU1BEjGyXiH/dt26+G9JClBrJgfWHePG9Num6I1KRDNGtMUNtalu7T0ZDHQGiyWLxdQ3m4h1p4KhQkYwBqyRRS4WNuJS6XXngwK+DNtfa5MbNL6+T1HCUfHNn6eJOkv9eUNajxWb1+sbewZ0Nltltvla9maufCYfV5w8gEwGpUKtfh4tWbX3+Oqda7ykaMKT6Ur4PYjIz4d3EsW72sEVlsQQIZKx+rl86IgfQ6Dm4QVHzMvJjY2CbHqHNopDjaNeVy3fCLr0OrGXN9oBhNa8VoWH63I2dgt1Lo/41nN9HUOY7jm4tvTdrZGs+5S4r5rErDE5rCvdKzuwolcToKbijWPYN2NuXCZPUO78ZhZtCzvztY9rCf3bbxgRaGW2kLOCAKXgtNCqob39itM0QEtcybriPV0O/bxK8PBUglYoqf0nn/B+nVn9fSeJ+RkeFOx4mNiYuNuTDIXF898kZvPQNni63p96ZhbJ+M/ESshJtDOB+ZXrTfFK7Q6ZWFVFcCmD+npmRvG7Em8U9oorEAvluRmdQGSIQGy0g26NRkrLvaGI/+F2V3z2JWfb7btNzid1oI1PlaUCaocnnVEC8NpWTbXP7W7tLnuiou5Lpe5Kmo93ktTcOcgYu2XjxX0lzEM8oc13Pt+UVY69PuliYGNwoqLP75kyZIDB5aQHE/UOz/WzrsCJs6JUlEaE1xp187o4buFIVrM5jduh31yu8lGknfZ6QUFBfrGYnUDPm2kYnUGkpA+AXLTRKx4xIqJXzMrHt/14J1Hl6oNemfOmitgUak5LQ1albU7/fK5w64jB1SeS0EzmxvqGz7cPL/l6+vXR7LTMzkrXFmNwerp/U+JWMKhbWRAi2IRK6xWUlJSQkJyRkZWVjV+q1ObdwOtKDViYdDqVLt9X15fHSNoCaHpe5X3CdbhI3uzo4RiHbzWFKwhUrFGA5f+LYw1jZ9DYpWczGPps4gVMnFaFAOn9uz7grePXsVuaX21mHMNH9BqL1pZ038WSz6WdwUFSsXqKKy4FsbitOK4aiXz1UrNKJkNCkqlJlrEyrztKlpZrVGZz41ES+Wj9ZVg4Q2AOIV6Dgut9l+VjRUMXDpIxOoLXEK6tnSz+GohllAtQ0KJFtS41/nQkLst8wtK4a2QUZnvjPYrqCWUS03T515v4YaQFCv14MGDifsxG2Rj9Rc+RZWIFej9uLDFx1CYQxxEgpUXV6IDlZLitSiaLiZWzo/OnNW8ltH2i9aMpYi1Kn19wWo9sUKsrVvlY/XgV5BUrCHCfm9prDXTfsGq3l43HyilUtCioXiblVilLto0Iy/Tmm31aqnVglbletKsBbixBCv5WMJfHxIgEavPYOAyvsWbhVo/Xw+zPqOVMgKxOC4aSK8+otVuHdCVOdaobKFbKIlenNbN1Sut67kpTGw0VghggqRitQU+nZoPq+vQP7FuTVvjW62Mz3VpEIHhtWh61yT9R9KrjfNBY4IZXq1TvlpwPgenEK28xZKP1ZPfWFKxgoFPULNhtQnpL4I1zYuVcaNuE6SkeLVoOn9qqtNJrDaBShNhgfs56VZub5lRi8eKRK3KvAVCsQ5tbRRWKGDaSsYKBD7BzYQ1tFtHGCCKxWsll9StA/zalAtaMRdLUp21tc7qjTvQSqOJUKDW+kzr1XdGh6ClELSyyHoXrI4e3ScTqyt/xpKMNRD4dPs/WK1Dx48bhgkNDR05duyI7l0Gk+9Y/WDNWTOHYG1HrOSSncQqPIXXoumndQanXu/M2o1WuMB4rTw8qXu1OCxuEhsyUonVdWIlH2scd3iXjjXuv2GJpnOAP6xZRItgJZTs3ARh4SScFfO0Lq8WrTJ2L8ReUZyWUg33s1LvFNw5xmmp1BiixcC3BANiHUIr+Vh9ufOlDKxQ4DO8+bBGiGNN2x6TcAF7FcZjYbto5lFdtT41pzZ590xAKK5a2C3UyjDc0ROtXADU4kLR8C0pMfE6sRLHEjl4thnMLSw5WP95DIfhGI4cO2JI9879vHfCDfOPNWvOHMRKIlbTw7xatOnZheocg6E2YfdcoDBCszit5DxOa62vFnZrO07h0b9h+f9UZUAI9yZPFlYzLvg+vYaTKe/h59udCTxWfByxiuaxBKssQ54hJ2n3ClAqfbSUKaqfWsVeLRWlJFo81vLlG0aJY0GrTh18Rq7DeJ/bGOTurPbNgCWcZNr7uaf0e3v3HtPUFccBvA5UQAVF5KHIxiNOxDGmbNO5V7Zl2xFsSxdaHl2REirl2W5NZQ25a8LG2pJhA+UP00CLDWw0C/+IIAOSZlBdIMOA2jiGJASMCNHEZVnGf/ude/tg0Htts0kW6fefTZOCfvydc3/33nMOAsACrU9t5lokLMFYbMrK3NI13fVZkVGNeNkeLfhfl9avP2Q0WEELj1AI1nq8gqkgP7/KgAU5FBGfFB528GD48QPkH3Y/yz+sdEQl6SlhwTfYvh4r8YQGY+XZzAqOnMKCYKum6enpz9qxFWBBPFo8Umv6h8/OZRCUFhWeDrQorG+ZsTxhuB9m7rOe8r3hfm8jPBmwyivO2sz1BqVcyKe0OFlL9qqWlpauFSMfcakGFSIWZ3u0Zktbzp37/QLW4rgYuQb0eBxbnf822S8slOonVhiicuBpYQV7w0p5CypLYzOruCalXE2VFkc2ZC9taWqZHjeWgBUOaVXD02ETl1ZZ0++/j4FWB2iBIv5ddhb6YxxjfZfCMMHv3wrLPfcn7Nt3JDIkAlE56B/WPpdy8FPC2uPtT/RCZjm2ui5UmJRSNS6tEtKqCZ7VDBvZrnYeuBBvaCEHWHDg16BVVDVGaXE45O/JuFx+DeeP8fNfjn7xnI+tQ+xORGWHX1jRLqwjTwkrNtXLNBqXOamxEX3yVoVeKZWDFlhZ7GVVpVVgJYMW1aWFsha1xEgOynYlB822U1p1UFtgBVgOvpDUGvuA5WtTmoBoGgomLM9i3vSngEWbuJhJG/GXshKwyNISih0We1EpPH6wGblgBZFBeKhmcXwsVzt4xvP4JovUGnPWFk8GhcXm8+Uy8Y/DP33MhOV9IduLfmGludqQDcSC3mG+9zVlpRNLrhbzsRU8YLZRdUVFhmRL47/8eS5Xu1BDaZ2G4NoqBa1LRF23WExaCYVqqUP8xW+JvmJBXNNWlB9YnvuUHRuJlXhiTtkHWJSWQQhWZfCmx2bMRiVuLORYGn8Aj2rOibQLPFIL91bZMG+tlI2OjoLWiFiH60qoVsulDt3jOD+wDiOGexfmFxaQ4xuIBZfDx62VEBIrWx5qb6escsAKQlnxh4YvPnjwgNRalCF88cvJcWrlObV0hhKwkkN6OB+xmLDoXkajaJ+xPJNWUOwGYr0QswRUVGmxpaFEe1F7UZndeAbx2WS4bKiroeGvL168+BVo/e7Swnc48B80O94+OnreqK3rNpzBdSWXSqVZLzFh7WKtTSryXCd9XcyW7hIO+w+wdvu8ie7Ea87SUptCiWKwKrIbTyMh2xVcV11fUxvqHjzAWlyUgysLsHg1oFV8ntQayeHBGIQ4TCl+YXnKJJ4ZK9rbHpWd/wYryj8sGIdvAxZEqRAQxe3t7UX2IQPHfefDRUKLbboLAlwXL2It66IDtMjKgumL1DpPavHY2KpH9xGLEYuht9/mY2VBItyf2UCsuJgh0kqvKiSqi0HLPqQTg5VTC8lDbS3T0yQXpTUmsl52wAtrCgs6sNnhajj0B7QGHXypskfKe4kRawstByTSZ6xI5MrhjcPC18PWvkp9ZSFBPtiyZ+gM6hJn2Eiaa2tpacorbi+d7pquam8v6/oGtJbYoEVhcWWgVQFak1rJIF/Y0+NQvMC4u2sX7f4bupcWYV5X/rl31r24UViQlMzF1j4FWOHHD8X2oawcuVDothLYmprG7bmWjEmb5uHU0lJGwcrXY7nWJRnKyiaxZGw21rp6ldJSNxreYPmL5dmfhXbRY6H1yyT9fyEWG7JnNZa/9+NxMMXr+wqJcrCqtg9l85RqtRBz8UuQUmCrqrITA7DBntd8bYlfI0fq+7aWsSnrEszy2WRlsUtKkGJ4/OrVC5MwEoVsZfITsGIZNxWm+bzD4kVEU45MOfKP54xJ/mLBFH+/slCbXw5axBCbbZLKQQtzoZ5Cc2mpuWFhroQ/chpNDCCVCtWI+4e7RkGLjc7wwAqw+MJVWmK6wmLt92AxTVtJtFhbabpZ+JJ+NAure/50v7HiXrZYiPxy0CKG1HK9UikFLrDiNPaC1bBVhdoW+gcX5tD17raOiWWEFlZAq26Ij05zwQpjqTmgdeECaFk71hSWz8uE4mlGFf1GpxCa91ZMiUCH12PtZvlTWlP5OMSQ0tSqNzm1OLXz5rLSIm3hrF7e/dpp/f2BTtUnwg7YN9443DJ9ntTKBixsJVWKKa3c3pMsuiQgModosFhbEM2+wii60XYc+TvJ7/lnX3aMESs6xFsX/7KgvACsLCYF9KaUllpcO2+Hl9TF2ob7Hf0jJQh1drdxEVJM1EqXjfAc9bderCWDwhJCL6ps1LVirUtfJtOfJODeREHbWni57EHSaW+a0/zV2v7POTGeESsdeT2gNHOyvEBr0c/2Vbq0DPXz5iLoUIutDXqdY6FDruivbetQ89tMCC0/bGqitIQcLmVlatRnYa2xdxnm1nXzNN2w2rXH152sez0bxaN9wTqwZicr0wUavjjNBbGcsOhbHwEWpXVaZQYrjEVI5tTN9XNz/SrEbRtQmQaU6JatpQmCteTiEsoKzizIaR2u+B56LFoLmgLxcsMXwbBHmu6SGBTiy54BFLbm0wzFHuS94lJi5jNmwarPqcW+Tpjz4D01cI3X1Q21omWFox7JZVl3EJpomzHDY+cqUssizVFTVvpaRU3l6OvMm/2etJUyzeuoeoWpnzqK3End86QZ69Ca8oxg+veLprlXfS75rcezjwALAlrSAcJ8trq6GnMVP5RI/lB2XnfcVDar5BP1E4P9aM5e1lQFXJ+CViNXaTKBFWDVcz5ibJdpCsRrW57mpXrCn7ReYWcYo1XwlrWvOLYw/TMco1sr98LzJ+YqXVr6OcKsASun1nCdZAb2qs4MzlyBDav3VGKEOuylVXhRJdZaLlGSVrUKZWsyiyHHfGi4gw+5ayt23V8pimZs7fRwHWAYiwm71tyZQqUxtB776VcHpMRYHvW9RnLpO8AK3/vgYC2z9fK9wcHLt27f6qw3nT4j40CrZQcs4PoctGrVJtKqUfk6iyl7fXmNvDUIObN7h3s4POGV6uF45MkWmoNtth5b9zXAg74TDgkihyzdQFzse4S19N1ac0U57ucrnMU1VTdSr1INNHffEiKdIaezFiF0yVZVWkZphdbLG8FKIX+D+a7MrcC8FclTJ+Frni8cor/QRqBViYiKDP7nt94RluZtKIet+kx4QnCsq+Ci94XvZXwrGZd44m2sNTuiNZdDXFygVS65pTb1KIW1M3dQjczAb2tEiP1wpZRcsnsWa0n1CoV0Lo5RIdzHG9/gF9cMKs+voxgutXvR6gRtSTu+LTwMHzCTFL93F3In2ssM6Tlqa/fuA/jkKeZXR1RreuJx36PW+2CVD1nFVQHH+nCFfLZu8LLYkC3jcPt7EEzygAUhtVRKhakykYkgGqzcSd8XzMh6yDOo0kOOI09Sj0TT97zx6MmJXz0IX0HMSWM6Oz8mdA5b5RcUuLWAC7CmJDc+4fP5OfKZNmTI5iJ+vxTliFbKqGh66wTXpQqGCStkJ1qXoJ0h9MUVvjcI0SWI9poXfTBtJ2JKhOeTRw8h5jC/7oZpK3NpgQArnHwcSguilVzXgRZHdbcNIXE2ErbJ0R/2siIcUiu08gOmeSgpadvBkJDIIwkJeIFDyPawbVHpSTsYB2MCnAfmTqT7g+Hwwf0MndS+8KMR3hx27U3dvvobhiRFhW0PiYzclwCrLuArHwwjz4OCFbhHX6GSCi0Os1Zvb4FIJFrLVV4guSYV8/lcjmrmxh05h8PhN7OltvYiMp+WaeZ732f9f3J4B5wylnQs/igkPjV928HIhK2xrP86cYlvaQpyRWu5QKtXcpeLZGwZEt68d+9GLeJwexyXxovyyHyaV/XeM3pcPkOgN31fI8rNzV3DBYEjFO+wdQYDQujMyDUV4sBKEVselbMt7zz7P7vCq9bJCtBaz5UPh5nODPY3d14faG6+d7cTtDrseeSe4eKmd57JHyzgk5ZGNEVyQTxc+SJtw83BezcuXx68fY/f3DCCAOsszua1cmt5qguCtQrqbnNgHbxUilR1N3s6795GA4QGUr2Jrch5y1gw5eTyjEaR9lo9r+emXIhGJHCWkVxye/EhafXeppyv3FqJmS4tj1e+iJDcEc9IllGzlpBI2sTdknmNpqK66V3W5g7WmpwSrK4u0UN8JuCgZMbQqTXnwmR/p80OE5lm9BRrsycuOSbUOCUQCJzFldsrEFjvLvdcuebQXakT5U/N1921wBuhs+dfZwXyXMrLJ425AhfXFAFntvVM3JyodwgbB3pFBfA7vZMFn2cksgIBLXKaLywsJL0E89Yrt69cu9ZwpaHh7o3Q3AKIKP/qe8/yjwXzd+IKNQoKKa/CeWudliC0VqtVS0yJcDRXA0PQk7iUGBiKhTiUmTPUNHYh401WIKsnruczLZMeKIoKa00ZL5zaxJ0o7VXxpEVUSEbgiejSh4Gy8lJcMHOdtODicophM+Ol1zd1006fODwWV3EB1alkViC0Y/Hl90MtoRSVxXIq0FsxjkXgyjwZaoF8GKDygSslMSYz89TzgS7UR6+4wKweSCCBBBJIIJs2fwP/leem+whKuQAAAABJRU5ErkJggg==]]]]
