#!/usr/bin/python3
from decimal import Decimal
from gmpy2 import mpz, mpq, mpfr
import numpy as np
import os.path
from PIL import Image as im
size = 480
max_xsize = size

bpath = "frames2"
path = bpath + "b"
if not os.path.exists(path):
    os.makedirs(path)

colors_file="colors2{}.gz".format(max_xsize)
if not os.path.exists("colors{}".format(max_xsize)):
    colors = np.zeros(        
        (max_xsize +1,
         3),
        dtype=np.uint8
    ) # create a simple square
    
    for x in range(max_xsize):
        color = list(np.random.choice(range(127,255,10), size=3))
        colors[x] = color
            
    with open(colors_file,'wb') as o:
        args = np.savez(o, colors=colors)

n = mpz(0)
digits = [str(c) for c in range(10)]
res = ""
scale= mpz(10)

def render(n, page):
    imgd = np.zeros((size,size,3),dtype=np.uint8)

    for i,l in enumerate(page):
        for j,c in enumerate(l):
            if c == '.':
                continue
            x = int(c)
            color = colors[x]    
            imgd[i][j][0]= color[0]
            imgd[i][j][1]= color[1]
            imgd[i][j][2]= color[2]
    return imgd
with open("big.txt") as fi:
    line = ""
    for l in fi:
        page = []
        for c in l:
            if c not in digits:
                continue
            if len(line) >= size:               
                page.append(line)
                if len(page) > size:
                    page.pop(0)
                np1 = render(n,page)
                data = im.fromarray(np1)
                name = '%s/%s_%08d.jpeg' % (path, "scan", n)
                data.save(name)
                print(name)
                line = ""
                n = n  + 1
            line = line + c
