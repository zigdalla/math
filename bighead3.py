#!/usr/bin/python3
from decimal import Decimal
from gmpy2 import mpz, mpq, mpfr
import numpy as np
import os.path
from PIL import Image as im
maxsize = 20000
maxsquare = maxsize * maxsize

size = 480
bpath = "frames4"
path = bpath + "d"
if not os.path.exists(path):
    os.makedirs(path)

## load colors
max_xsize = 10    
colors_file="colors2{}.gz".format(max_xsize)
if not os.path.exists(colors_file):
    colors = np.zeros(        
        (max_xsize +1,
         3),
        dtype=np.uint8
    ) # create a simple square
    
    for x in range(max_xsize):
        color = list(np.random.choice(range(127,255,10), size=3))
        colors[x] = color
            
    with open(colors_file,'wb') as o:
        args = np.savez(o, colors=colors)
else:
    colorsd = np.load(colors_file)
    colors = colorsd['colors']
    #print(colors)

digits = [str(c) for c in range(10)]

scale= mpz(10)

def render(n, page):
    imgd = np.zeros((n+1,n+1,3),dtype=np.uint8)
    lp = len(page)
    for i,l in enumerate(page):
        ll = len(l)
        for j,c in enumerate(l):
            #print(l)
            if c not in digits:
                continue            
            x = int(c)
            color = colors[x]
            c1 = color[0]
            if j > n:
                print(i,j,n,lp,ll)
            imgd[i][j][0]= c1
            imgd[i][j][1]= color[1]
            imgd[i][j][2]= color[2]

    return imgd

res = ""
with open("bigout.txt") as fi:
    for l in fi:
        res = res + l
    print("going to process",res[0:10])
    stop = len(res)
    line = ""
    print("Len",stop)
    m = 0
    block = res[:maxsquare]
    
    for linesize in range(10,maxsize):
        start = 0
        page = []
        print("linesize", linesize)
        for linemax in range(0, linesize * linesize, linesize): # take the square in steps of linesize
            line = block[start:linemax]
            page.append(line)
            start = linemax

        name = '%s/%s_%08d.jpeg' % (path, "scan", linesize)
        if not os.path.exists(name):
            np1 = render(linesize,page)
            data = im.fromarray(np1)            
            data.save(name)
            print(name)

